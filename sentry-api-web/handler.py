import json
import sys, os

# get this file's directory independent of where it's run from
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "vendored"))

# import the shared library, now anything in common/ can be referenced as
# `common.something`

import datetime
import re
import requests
import time


#Sentry
domain = 'https://sentry.io'
auth = '0d897c5878ed4263b90518e931c065cbef1c5b5d4298416bbe49554257728a59'

event_id = '0399e1f473804378b65da536aeec2ec3'

api = domain + '/api/0/projects/justika/justika-api/'
web = domain + '/api/0/projects/justika/justika-web/'
api_issue = api + 'issues/?statsPeriod=24h&query=error'
web_issue = web + 'issues/?statsPeriod=24h&query=error'


#Elasticsearch
es_domain = 'https://search-justika-kibana-r7uy4attg2byayssuanyccqsz4.ap-southeast-1.es.amazonaws.com/'
es_index_api = 'sentry-api/'
es_type_api = 'issues/'
es_index_web = 'sentry-web/'
es_type_web = 'issues/'


def do_main(es_index, es_type, hostname):
    #Fetch Sentry
    response = requests.get(
        hostname,
        headers = {
            'Authorization' : 'Bearer 0d897c5878ed4263b90518e931c065cbef1c5b5d4298416bbe49554257728a59'
        },
    )

    resp = response.text
    conv = json.loads(resp)

    length = len(conv)


    #Main work
    for i in range(length):
        #Check status
        level = conv[i]['level']
        if(level == 'error'):

            # obj_id = conv[i]['id'] + str_add
            obj_id = conv[i]['id']
            to_send = {}
            to_send['lastSeen'] = conv[i]['lastSeen']
            to_send['userCount'] = conv[i]['userCount']
            to_send['culprit'] = conv[i]['culprit']
            to_send['title'] = conv[i]['title']
            to_send['id'] = conv[i]['id']
            to_send['count'] = conv[i]['count']
            to_send['permalink'] = conv[i]['permalink']
            to_send['status'] = conv[i]['status']
            
            #Send to ES
            r = json.dumps(to_send)
            # print(r)

            put_test = es_domain + es_index + es_type + obj_id
            response_put = requests.put(
                put_test,
                headers = {
                    'Content-Type': 'application/json'
                },
                data = r,
            )

            # print(response_put.text)


def fetch_sentry_web(event, context):
    do_main(es_index_web, es_type_web, web_issue)

def fetch_sentry_api(event, context):
    do_main(es_index_api, es_type_api, api_issue)