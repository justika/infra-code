import json
import sys, os

# get this file's directory independent of where it's run from
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "vendored"))

# import the shared library, now anything in common/ can be referenced as
# `common.something`

import boto3
import datetime
import re
import requests
import time

from requests.auth import HTTPBasicAuth


#Elasticsearch
# es_domain = 'https://search-intern-justika-w5nrc3knmr2hb3wa2pt35rjabu.ap-southeast-1.es.amazonaws.com/'
es_domain = 'https://search-justika-kibana-r7uy4attg2byayssuanyccqsz4.ap-southeast-1.es.amazonaws.com/'

#Elasticsearch index and type
es_lambda_duration_index = 'lambda_duration/'
es_lambda_duration_type = 'duration/'
es_lambda_invocation_index = 'lambda_invocation/'
es_lambda_invocation_type = 'invocation/'
es_rds_cpu_index = 'rds_cpu/'
es_rds_cpu_type = 'cpu/'
es_rds_connectivity_index = 'rds_connectivity/'
es_rds_connectivity_type = 'connectivity/'

#Elasticsearch index and type
es_rendertron_type = 'rendertron/'
es_api_type = 'api/'
es_admin_type = 'admin/'
es_production_type = 'production/'

es_uptime_rendertron_index = 'uptime_rendertron/'
es_uptime_api_index = 'uptime_api/'
es_uptime_admin_index = 'uptime_admin/'
es_uptime_production_index = 'uptime_production/'

es_downtime_rendertron_index = 'downtime_rendertron/'
es_downtime_api_index = 'downtime_api/'
es_downtime_admin_index = 'downtime_admin/'
es_downtime_production_index = 'downtime_production/'

es_response_rendertron_index = 'response_rendertron/'
es_response_api_index = 'response_api/'
es_response_admin_index = 'response_admin/'
es_response_production_index = 'response_production/'

#Account
email = 'technology@justika.com'
password = 'HuSe1n1300!'
app_key = 'fmmqzkcxnpxxy1wfq93d9w9014u5c003'
#Format date "2019-08-15T18:51:20.022Z"

#Rendertron
id_justika_com = 4853218
id_api_justika_com = 4853225
id_admin_justika_com = 4853226
id_rendertron_justika_com = 4961282

api_domain = 'https://api.pingdom.com/api/2.0'
check = '/checks'
summary_average = '/summary.average/'
summary_performance = '/summary.performance/'

summary_average_id = id_justika_com
summary_performance_id = id_justika_com

timenow=datetime.datetime.now()
timebefore=datetime.datetime.now() - datetime.timedelta(hours=1)

client = boto3.client(
    'cloudwatch',
    aws_access_key_id='AKIAIAXKGHMXGG5CPY5Q',
    aws_secret_access_key='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',
    region_name='ap-southeast-1'
)

starttime=datetime.datetime.utcnow() - datetime.timedelta(hours=1)
endtime=datetime.datetime.utcnow()


def fetch_cloudwatch_do_lambda(namespc, metric, dimens_n, dimens_v, stat, unt, index, type_cl):
    namespace=namespc
    metric_name=metric
    dimension_name=dimens_n
    dimension_value=dimens_v
    period=300
    statistics=stat
    unit=unt
    scanby='TimestampDescending'

    response = client.get_metric_statistics(
        Namespace=namespace,
        MetricName=metric_name,
        Dimensions=[
            {
                'Name': dimension_name,
                'Value': dimension_value
            },
        ],
        StartTime=starttime,
        EndTime=endtime,
        Period=period,
        Statistics=[
            statistics,
        ],
        Unit=unit
    )

    #Main work
    length = len(response['Datapoints'])
    for i in range(length):
        
        obj_id = str(response['Datapoints'][i]['Timestamp'])
        obj_id = obj_id.replace(" ", "")
        
        datetime = response['Datapoints'][i]['Timestamp']
        str1 = str(datetime)[0:10]
        str2 = str(datetime)[11:19]
        datetime_object = (str1+'T'+str2+'Z')
        
        to_send = {}
        to_send['Timestamp'] = str(datetime_object)
        to_send['Average'+metric_name] = response['Datapoints'][i][stat]

        #Send to ES
        r = json.dumps(to_send)

        put_test = es_domain + index + type_cl + obj_id
        response_put = requests.put(
            put_test,
            headers = {
                'Content-Type': 'application/json'
            },
            data = r,
        )

def req_get(hostname):
    response = requests.get(
        hostname,
        headers = {
            'App-Key' : app_key,
        },
        auth = HTTPBasicAuth(email, password)
    )
    return response

def req_put(hostname, data_key, data_value, timenow):
    str1 = (str(timenow))[0:10]
    str2 = (str(timenow))[11:19]
    time_to_send = str1 + 'T' + str2 + 'Z'

    data = {data_key : data_value, 'Timestamp' : time_to_send}
    r = json.dumps(data)

    response = requests.put(
        hostname,
        headers = {
            'Content-Type': 'application/json'
        },
        data = r,
    )
    return response

def fetch_pingdom_do_lambda(summary_average_id, es_type, uptime_index, downtime_index, response_index):
    #Fetch uptime downtime
    unixnow = time.mktime(timenow.timetuple())
    unixbefore = time.mktime(timebefore.timetuple())
    
    param = '/?includeuptime=true&resolution=hour&from='+str(unixbefore)+'&to='+str(unixnow)
    hostname = api_domain + summary_average + str(summary_average_id) + param

    response = req_get(hostname)

    conv = json.loads(response.text)
    uptime = conv['summary']['status']['totalup']
    downtime = conv['summary']['status']['totaldown']
    response_time = conv['summary']['responsetime']['avgresponse']
    sum = uptime + downtime

    uptime = 100 * (float(uptime)/float(sum))
    downtime = 100 * (float(downtime)/float(sum))

    obj_id = str(timenow)
    obj_id = obj_id.replace(" ", "")
    put_uptime = es_domain + uptime_index + es_type + obj_id
    put_downtime = es_domain + downtime_index + es_type + obj_id
    put_responsetime = es_domain + response_index + es_type + obj_id
    
    #Send to ES
    up = req_put(put_uptime, ('AvgUptime'), uptime, str(timenow))
    down = req_put(put_downtime, ('AvgDowntime'), downtime, str(timenow))
    resp = req_put(put_responsetime, ('AvgResponsetime'), response_time, str(timenow))

#Handlers
def fetchCloudWatch(event, context):
    fetch_cloudwatch_do_lambda('AWS/Lambda', 'Duration', 'FunctionName', 'justika-production', 'Average', 'Milliseconds', es_lambda_duration_index, es_lambda_duration_type)
    fetch_cloudwatch_do_lambda('AWS/Lambda', 'Invocations', 'FunctionName', 'justika-production', 'SampleCount', 'Count', es_lambda_invocation_index, es_lambda_invocation_type)
    fetch_cloudwatch_do_lambda('AWS/RDS', 'DatabaseConnections', 'DBInstanceIdentifier', 'production-justikadb', 'Sum', 'Count', es_rds_connectivity_index, es_rds_connectivity_type)
    fetch_cloudwatch_do_lambda('AWS/RDS', 'CPUUtilization', 'DBInstanceIdentifier', 'production-justikadb', 'Average', 'Percent', es_rds_cpu_index, es_rds_cpu_type)

def fetchPingdom(event, context):
    fetch_pingdom_do_lambda(id_justika_com, es_production_type, es_uptime_production_index, es_downtime_production_index, es_response_production_index)
    fetch_pingdom_do_lambda(id_api_justika_com, es_api_type, es_uptime_api_index, es_downtime_api_index, es_response_api_index)
    fetch_pingdom_do_lambda(id_admin_justika_com, es_admin_type, es_uptime_admin_index, es_downtime_admin_index, es_response_admin_index)
    fetch_pingdom_do_lambda(id_rendertron_justika_com, es_rendertron_type, es_uptime_rendertron_index, es_downtime_rendertron_index, es_response_rendertron_index)