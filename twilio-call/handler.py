import json
import sys, os

# get this file's directory independent of where it's run from
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "vendored"))

# import the shared library, now anything in common/ can be referenced as
# `common.something`

import datetime
import re
import requests
import time
from twilio.rest import Client


def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

# es_domain = "https://search-intern-justika-w5nrc3knmr2hb3wa2pt35rjabu.ap-southeast-1.es.amazonaws.com/"
es_domain = 'https://search-justika-kibana-r7uy4attg2byayssuanyccqsz4.ap-southeast-1.es.amazonaws.com/'
es_index = "twilio_call/"
es_type = "call/"


account_sid = "AC42478d4b6007f751f602916289d39c0a"
auth_token  = "4eca9082dbae6cb667c85bd144c05711"

def twil_main():
    client = Client(account_sid, auth_token)

    time_x=datetime.datetime.utcnow() - datetime.timedelta(days=1)

    calls = client.calls.list(
        start_time_after=time_x
    )

    for call in calls:
        obj_id = call.sid
        call_time = myconverter(call.start_time)
        str1 = str(call_time)[0:10]
        str2 = str(call_time)[11:19]
        call_time_object = (str1+'T'+str2+'Z')

        data = {
            'sid' : call.sid, 
            'start_time' : call_time_object,
            'status' : call.status
        }
        r = json.dumps(data)
        
        put_test = es_domain + es_index + es_type + obj_id
        response_put = requests.put(
            put_test,
            headers = {
                'Content-Type': 'application/json'
            },
            data = r,
        )

def fetch_twilio(event, context):
    twil_main()