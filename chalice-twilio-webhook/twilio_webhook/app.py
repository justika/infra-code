import json
import datetime
import sys, os

here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "vendored"))

import requests
import urllib
from chalice import Chalice

app = Chalice(app_name='twilio_webhook')

es_domain = 'https://search-justika-kibana-r7uy4attg2byayssuanyccqsz4.ap-southeast-1.es.amazonaws.com/'
es_index = "twilio_webhook/"
es_type = "webhook/"

@app.route('/')
def index():
    return {'hello': 'world'}

@app.route('/users', methods=['POST'])
def create_user():
    user_as_json = app.current_request.json_body
    return {'user': user_as_json}

@app.route('/twiltest', methods=['POST'], content_types=['application/x-www-form-urlencoded; charset=utf-8', 'application/x-www-form-urlencoded'])
def test_twil():
    timenow=datetime.datetime.now()
    str1 = (str(timenow))[0:10]
    str2 = (str(timenow))[11:19]
    time_to_send = str1 + 'T' + str2 + 'Z'

    post_data = app.current_request.raw_body.decode(encoding='UTF-8')
    post_data = urllib.parse.parse_qs(post_data)
    
    # return {'shitsu': post_data}
    
    obj_id = time_to_send

    to_send = {}
    to_send['Timestamp'] = post_data['Timestamp']
    x = post_data['Payload'][0]
    to_send['Payload'] = json.loads(x)

    # return x

    r = json.dumps(to_send)
    
    put_test = es_domain + es_index + es_type + obj_id
    response_put = requests.put(
        put_test,
        headers = {
            'Content-Type': 'application/json'
        },
        data = r,
    )